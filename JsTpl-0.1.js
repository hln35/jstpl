/**
 * @name : JsTpl
 * @version : 0.0.1
 * @author: gudehsiao
 * @last modify: 2012-10-3 10:38
 * @note: any copy, but please indicate the source
 */
(function () {
    var JsTpl = function (sTpl, lTag, rTag, debug) {
        this.sTpl = sTpl || '';
        this.lTag = lTag || '{{';
        this.rTag = rTag || '}}';
        this.cTpl = null;
        this.debug = debug || false;

        this.hasSyntaxReg = new RegExp(this.lTag + '.*' + this.rTag);
        this.syntaxReg = new RegExp(this.lTag + '(.*)' + this.rTag);

    };

    JsTpl._syntaxes_ = {
        'if(.*):':new RegExp('^if(.*):$'),
        'else if(.*):':new RegExp('^else if(.*):$'),
        'else:':new RegExp('^else:$'),
        'endif;':new RegExp('^endif;$'),
        'for(.*):':new RegExp('^for(.*):$'),
        'endfor;':new RegExp('^endfor;$')
    };

    JsTpl.prototype._transItem_ = function (item) {

        if (this.hasSyntaxReg.test(item)) {
            var that = this;

            return item.replace(this.syntaxReg, function (m, p1) {
                var syntax = that._getSyntax_(p1.replace(/^\s*|\s*$/g, ''));
                var part1 = '';
                var isDefOutput = false;

                switch (syntax) {
                    case 'if(.*):':
                        part1 = p1.replace('):', '){');
                        break;
                    case 'else if(.*):':
                        part1 = p1.replace('else', '}else').replace('):', '){');
                        break;
                    case 'else:':
                        part1 = p1.replace('else', '}else').replace(':', '{');
                        break;
                    case 'endif;':
                        part1 = '}';
                        break;
                    case 'for(.*):':
                        part1 = p1.replace('):', '){');
                        break;
                    case 'endfor;':
                        part1 = '}';
                        break;
                    default :
                        part1 = p1;
                        if (part1.indexOf('=') == -1) {
                            isDefOutput = true;
                        }
                        break;
                }

                if (isDefOutput) {
                    return 'output += ' + part1 + ';';
                } else {
                    return part1;
                }

            });
        } else {
            return "output += '" + item.replace(/'/g, "\\'") + "';";
        }
    };

    JsTpl.prototype._getSyntax_ = function (str) {
        var syntaxes = JsTpl._syntaxes_;
        var retval = '';

        for (var p in syntaxes) {
            if (syntaxes.hasOwnProperty(p)) {
                if (syntaxes[p].test(str)) {
                    retval = p;
                    break;
                }
            }
        }

        return retval;
    };

    JsTpl.prototype.splitItems = function (str, lTag, rTag) {
        var lTagLen = lTag.length;
        var rTagLen = rTag.length;
        var items = [];
        var lastJ = 0;

        for (var i = 0, len = str.length; i < len; ++i) {

            if (lTag == str.slice(i, i + lTagLen)) {

                for (var j = i + lTagLen - 1; j < len; ++j) {

                    if (rTag == str.slice(j, j + rTagLen)) {

                        items.push(str.slice(lastJ, i));

                        items.push(str.slice(i, j + rTagLen));

                        j += rTagLen;

                        lastJ = j;

                        i = j - 1;
                        break;
                    }
                }

                i += lTagLen;
            }
        }

        items.push(str.slice(lastJ, str.length));

        return items;
    };

    JsTpl.prototype._trans_ = function () {
        var fnBody = 'var output = \'\';';

        var items = this.splitItems(this.sTpl.replace(/^\s*|\s*$/g, '').replace(/\r{0,1}\n/g, ''), this.lTag, this.rTag);

        if (items.forEach) {
            items.forEach(function (e, i, a) {
                fnBody += this._transItem_(e);
            }, this);
        } else {
            for (var i = 0, len = items.length; i < len; ++i) {
                fnBody += this._transItem_(items[i]);
            }
        }

        fnBody += 'return output;';

        if (this.debug) {
            console.log(fnBody);
        }

        var transed = new Function([], fnBody);
        this.cTpl = new JsTpl.CTpl(transed);
    };

    JsTpl.prototype.getCTpl = function () {
        if (this.cTpl == null) {
            this._trans_();
        }

        return this.cTpl;
    };

    JsTpl.CTpl = function (transed) {
        this.transed = transed;
    };

    JsTpl.CTpl.prototype.render = function (context) {
        return this.transed.apply(context);
    };

    window.JsTpl = JsTpl;
})();